# FollettIce Theme
`Drupal 8 Theme`
This theme illustrates the use of the `gulp.js` file in the code sample repo. All of the templates and front end implementations were done by me. My folder structures are always evolving as I learn from every project. Partials in the `core` folder are global partials. Each front end component is broken out into its own individual `modules` folder with corresponding partials.
**Since then I've renamed `modules` to `components` to be more platform agnostic.
