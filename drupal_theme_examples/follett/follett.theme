<?php

/**
 * @file
 * Functions to support theming in the Follett theme.
 */

use Drupal\taxonomy\Entity\Term;
use Drupal\views\ViewExecutable;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;

use Drupal\follettice_custom\Filter\Find;
use Drupal\follettice_custom\Exception\HttpException;

function follett_preprocess_node__product__full(&$vars) {
    if (!isset($vars['node'])) {
        return;
    }

    $node = $vars['node'];
    if (!($node instanceof \Drupal\node\Entity\Node) || !method_exists($node, 'getFields')) {
        return;
    }

    $fields = $node->getFields();
    $targets = [];

    if (array_key_exists('field_related_model_numbers', $fields)) {
        foreach ($fields['field_related_model_numbers'] as $item) {
            if (!($item instanceof \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem)) {
                continue;
            }

            $value = $item->getValue();
            if (array_key_exists('target_id', $value)) {
                $targets[] = (int) $value['target_id'];
            }
        }
    }

    $vars['targets'] = $targets;
    $vars['node_id'] = $node->id();
    $filters = [];

    foreach ($_GET as $key => $value) {
        $mod_key = strtolower($key);

        if (substr($mod_key, 0, 7) == 'filter_') {
            $filters[substr($mod_key, 7)] = (int) $_GET[$key];
        }
    }

    try {
        $finder = new Find($node, $filters);
        $finder->populateFilters()->fetchModelNumber();
    } catch (HttpException $e) {
    }

    $vars['finder']['fields'] = $finder->getFilterableFields();
    $vars['finder']['filters'] = $finder->getData();
    $vars['finder']['selected'] = $finder->getSelected();
    $vars['finder']['models'] = $finder->getModelNumbers();

    $vars['#attached']['drupalSettings']['vsf'] = $vars['finder'];
}


/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 */
function follett_theme_suggestions_block_alter(array &$suggestions, array $vars) {
  // Block suggestions for custom block bundles.
  if (isset($vars['elements']['content']['#block_content'])) {
    array_splice($suggestions, 1, 0, 'block__bundle__' . $vars['elements']['content']['#block_content']->bundle());
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function follett_theme_suggestions_page_alter(array &$suggestions, array $variables) {

  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $is_node = is_object($node);

    if ($is_node) {
      $content_type = $node->bundle();
      $suggestions[] = 'page__' . $content_type;
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function follett_theme_suggestions_taxonomy_term_alter(&$suggestions, $vars) {
  if (isset($vars['elements']['#view_mode']) && isset($vars['elements']['name']['#bundle'])) {
    array_push($suggestions, 'taxonomy_term__' . $vars['elements']['name']['#bundle'] . '__' . $vars['elements']['#view_mode']);
  }
}

/**
 * Implements template_preprocess_views_exposed_form().
 */
function follett_preprocess_views_exposed_form(&$vars) {
  $form = &$vars['form'];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function follett_form_views_exposed_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  switch ($form['#id']) {
    case 'views-exposed-form-service-agent-lookup-page-1':
      $form['field_postal_codes_serviced_value']['#attributes']['placeholder'] = 'Enter Zipcode';
      $form['actions']['submit']['#value'] = 'Submit';
    break;
  }
}

/**
 * Implements hook_form_alter().
 *
 * Modify attributes of search form.
 */
function follett_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $id = $form['#id'];
  $exposed_filter_id = $form['form_id']['#id'];
  if ($id == 'views-exposed-form-sales-literature-resources-resources-landing-filter') {
    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = 'follett_form_submit';
      }
    }
  }

  // Target the search page.
  if ($id == 'views-exposed-form-search-search-page-view') {
    $form['actions']['submit']['#value'] = '';
    $form['search_api_fulltext']['#attributes']['placeholder'] = t('Enter term to search');
  }
}

function follett_form_submit($form, FormStateInterface $form_state) {
  _follett_selective_filters_check_filters($form);
}

/**
 * Implements hook_views_pre_build().
 */
function follett_views_pre_render(ViewExecutable &$view) {
  $view_id = $view->id();

  if ($view_id == "search") {
    $view = $view;
    $header = $view->header;
    $query = NULL;
    $is_query = array_key_exists('search_api_fulltext', $_GET);

    if ($is_query) {
      $query = $_GET['search_api_fulltext'];
    }

    if (!empty($header)) {
      $header_result = $header['result'];
      $header_result_content = $header_result->options['content'];

      if (!empty($header_result_content)) {
        $header_result_content = '<div class="search__result-summary">' . $header_result_content . ' "' . $query . '"</div>';
        $view->header['result']->options['content'] = $header_result_content;
      }
    }
  }

  if ($view_id == 'model_number_finder') {
    $results = $view->result;
    $vocabularies = [];

    if (!empty($results)) {
      foreach ($results as $result) {
        $field_entity = $result->_relationship_entities['field_related_model_numbers'];

        if (isset($field_entity) && !empty($field_entity)) {
          $tid = $field_entity->id();

          if (!empty($tid)) {
            $term_obj = Term::load($tid);

            if ($term_obj->hasField('field_product_config')) {
              $term_obj->get('field_product_config');

              if (!empty($term_obj)) {
                $field_entities = $term_obj->get('field_product_config')->referencedEntities();

                foreach ($field_entities as $entity) {
                  array_push($vocabularies, $entity->getVocabularyId());
                }
              }
            }
          }
        }
      }

      if (!empty($vocabularies)) {
        $vocabularies = array_unique($vocabularies);
      }

      // Remove unnecessary exposed_filters.
      $view_exposed_filters = $view->exposed_widgets;
      $exposed_filter_name = 'field_id_';
      $exposed_filters_array = [];

      // Let's grab only array elements that are exposed filters.
      if (!empty($view_exposed_filters)) {
        foreach ($view_exposed_filters as $key => $exposed_filter) {
          $array_item = stripos($key, $exposed_filter_name);

          if ($array_item !== FALSE) {
            $exposed_filters_array[$key] = $exposed_filter;
          }
        }
      }

      if (!empty($exposed_filters_array)) {
        foreach ($exposed_filters_array as $key => $filter_item) {
          $stripped_key = str_replace($exposed_filter_name, '', $key);
          $display_filter = in_array($stripped_key, $vocabularies);

          if (!$display_filter) {
            unset($view->exposed_widgets[$key]);
          }
        }
      }

      // Remove unnecessary filters.
      $view_filter_items = $view->filter;
      if (!empty($view_filter_items)) {
        foreach ($view_filter_items as $elementKey => $filter_item) {
          $vid_option = $filter_item->options;

          if (array_key_exists('vid', $vid_option)) {
            $filter_vid = $filter_item->options['vid'];
            $display_filter = in_array($filter_vid, $vocabularies);

            if (!$display_filter) {
              unset($view_filter_items[$elementKey]);
            }
          }
        }
        $view->filter = $view_filter_items;
        $view->setExposedInput($view_filter_items);
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_paragraph__featured_video(&$vars) {
  $elements = $vars['elements'];
  $video_url = $elements['field_video_url']['0']['#context']['value'];

  if (!empty($video_url)) {
    if (strpos($video_url, 'youtube')) {
      $video_type = 'youtube';
      $vars['video_type'] = 'video-youtube';
    }
    elseif (strpos($video_url, 'youtu.be')) {
      $video_type = 'youtube_short';
      $vars['video_type'] = 'video-youtube';
    }
    elseif (strpos($video_url, 'vimeo')) {
      $video_type = 'vimeo';
      $vars['video_type'] = 'video-vimeo';
    }
  }
  else {
    $video_type = '';
  }

  if (!empty($video_type)) {
    if ($video_type == 'youtube') {
      $video_id = substr(strstr($video_url, 'v='), 2);
      $vars['video_url'] = '//www.youtube.com/embed/' . $video_id . '?enablejsapi=1';
    }
    if ($video_type == 'vimeo') {
      $video_id = substr(strstr($video_url, 'com/'), 4);
      $vars['video_url'] = '//player.vimeo.com/video/' . $video_id . '?api=1';
    }
    if ($video_type == 'youtube_short') {
      $video_id = substr(strstr($video_url, 'be/'), 2);
      $vars['video_url'] = '//www.youtube.com/embed/' . $video_id . '?enablejsapi=1';
    }
  }
  else {
    $vars['video_url'] = '';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_views_view__sales_literature_resources(&$vars) {
    _follett_selective_filters_prepare($vars);
}

function follett_preprocess_views_view__resources_tech_documentation(&$vars) {
    _follett_selective_filters_prepare($vars);
}

function _follett_selective_filters_check_filters($form = null) {
    $field_values = [
        'field_resource_industry_target_id' => 0,
        'field_resource_product_type_target_id_selective' => 0,
        'field_associated_products_target_id_selective' => 0,
        'field_documentation_type_target_id_selective' => 0,
        'field_language_target_id_selective' => 0,
        'combine' => 0,
    ];

    if ($form !== null) {
        foreach ($form as $key => $data) {
            if (substr($key, 0, 6) == 'field_' || $key == 'combine') {
                $field_values[$key] = $data['#value'];
            }
        }
    }

    foreach (\Drupal::request()->query->all() as $key => $value) {
        if (isset($field_values[$key])) {
            $field_values[$key] = $value;
        }
    }

    $tempstore = \Drupal::service('user.private_tempstore')->get('follett');
    $tempstore->set('sales_lit_search_params', $field_values);
}

function _follett_selective_filters_prepare(&$vars) {
    _follett_selective_filters_check_filters();

  $tempstore = \Drupal::service('user.private_tempstore')->get('follett');
  $field_values = $tempstore->get('sales_lit_search_params');

  $tempstore->delete('sales_lit_search_params');

  // grab non-model_numbers terms and pass that to the front end
  $vids = [
    'industries_service_sectors',
    'product_type',
    'documentation_type',
    'language',
  ];

  $query = \Drupal::database()->select('taxonomy_term_field_data', 'd1')
    ->fields('d1', ['vid', 'tid', 'name'])
    ->condition('d1.vid', $vids, 'IN');

  $terms = $query->execute()->fetchAll(\PDO::FETCH_NAMED);

  $query = \Drupal::database()->select('node', 'n');
  $query->fields('n', ['nid', 'vid']);
  $query->fields('d', ['title']);
  $query->condition('n.type', 'product');
  $query->join('node_field_data', 'd', 'd.nid = n.nid AND d.vid = n.vid AND d.status = 1');

  $products = $query->execute()->fetchAll(\PDO::FETCH_NAMED);

  // now re-order them as [tid => name] for easy lookup
  $pass = $labels = ['terms' => [], 'products' => []];
  foreach ($terms as $term) {
    $pass['terms'][(int) $term['tid']] = $term['name'];
    $labels['terms'][$term['vid']][(int) $term['tid']] = $term['name'];
  }

  // do the same for products but with nid
  foreach ($products as $product) {
    $pass['products'][(int) $product['nid']] = $product['title'];
    $labels['products'][(int) $product['nid']] = $product['title'];
  }

  // $vars['view_array']['#attached']['library'][] = 'follett/sales-literature';
  $vars['view_array']['#attached']['drupalSettings']['vsf'] = $field_values;
  $vars['view_array']['#attached']['drupalSettings']['labels'] = $pass;
  $vars['view_array']['#attached']['drupalSettings']['labels_ex'] = $labels;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_html(&$vars) {
  if (!empty($vars['node_type'])) {
    $vars['attributes'] = ['class' => 'node_' . $vars['node_type']];
  }

  // Add path class to body.
  $path_class = !$vars['root_path'] ? 'path-frontpage' : 'path-' . Html::getClass($vars['root_path']);
  if (isset($path_class)) {
    $vars['attributes']['class'] = $path_class;
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_input(&$vars) {
  $element = $vars['element'];

  if (isset($element['#value']) && !empty($element['#value'])) {
    $value = $element['#value'];

    if(!is_array($value)) {
      $value = 'input-type-' . strtolower($element['#value']);
      $vars['attributes']['class'][] = $value;

      $vars['val'] = $value;

      $vars['value_text'] = $element['#value'];
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_node(&$vars) {
  $theme = \Drupal::config('system.theme')->get('default');
  $node = $vars['node'];

  $funcs = [];
  $funcs[] = $theme . '_preprocess__node__' . $node->getType();
  $funcs[] = $theme . '_preprocess__node__' . $node->getType() . '__' . $vars['view_mode'];
  if (!empty($vars['view_mode'])) {
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'];
    $vars['theme_hook_suggestions'][] = 'node__' . $node->getType() . '__' . $vars['view_mode'];
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->id() . '__' . $vars['view_mode'];
  }

  _follett_apply_preprocess_funcs('node', $node->getType(), $funcs, $vars);
}

/**
 * Implements template_preprocess().
 */
function follett_preprocess(&$vars) {
  $vars['base_path'] = base_path();
  $vars['base_url'] = \Drupal::request()->getSchemeAndHttpHost();
  $vars['svg_path'] = drupal_get_path('theme', 'follett') . '/images/svg/';
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_field_collection_item__field_full_width_cta_callout(&$vars) {
  $item = $vars['item']['#field_collection_item'];

  if (!($item->isEmpty()) && $item->hasField('field_full_width_cta_link')) {
    $field_link = $item->get('field_full_width_cta_link')->first();

    if (!($field_link->isEmpty())) {
      $title = $field_link->get('title')->getValue();

      if (!empty($title)) {
        $vars['link_title'] = $title;
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_paragraph__full_width_cta(&$vars) {
  $paragraph = $vars['paragraph'];
  if ($paragraph->hasField('field_full_width_cta_link')) {
    $field_link = $paragraph->get('field_full_width_cta_link')->first();

    if ($field_link && !($field_link->isEmpty())) {
      $title = $field_link->get('title')->getValue();

      if (!empty($title)) {
        $vars['link_title'] = $title;
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_container(&$vars) {
  $element = $vars['element'];

  if (array_key_exists('#display_id', $element)) {
    $vars['block_display_id'] = $element['#display_id'];
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_page(&$vars) {
  $is_node = array_key_exists('node', $vars);

  if ($is_node) {
    $node = $vars['node'];
    $is_object = is_object($node);

    if ($is_object) {
      $bundle = $node->bundle();

      if ($bundle == 'resource') {
        if ($node->hasField('field_resource_location')) {
          $field_resource_location = $node->get('field_resource_location');

          if (!$field_resource_location->isEmpty()) {
            $list_items = $field_resource_location->getIterator();
            $values_array = [];

            foreach ($list_items as $item) {
              $value = $item->getValue();
              $value = $value['value'];
              $values_array[] = $value;
            }

            if (!empty($values_array)) {
              $vars['list_val'] = $values_array;
            }
          }
        }
      }
    }
  }

  // Add taxonomy_term object into page template.
  if ($taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
    $vars['taxonomy_term'] = $taxonomy_term;
    $vid = $taxonomy_term->getVocabularyId();

    // Used to determine whether to print banner image.
    if ($vid == 'product_type') {
      $vars['is_product_type'] = TRUE;
    }
  }

  // Get resource type -- determines page TPL for resources detail page.
  $vars['resource_type'] = resource_type($vars);

  // get website country code
  ip_country_code($vars);
}

function ip_country_code(&$vars) {
  if (\Drupal::hasService('smart_ip.smart_ip_location')) {
    $location = \Drupal::service('smart_ip.smart_ip_location');
    $location_country_code = $location->get('countryCode');
    $eu_country_codes = array("BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "UK", "PL");

    $is_eu = in_array($location_country_code, $eu_country_codes);

    if ($is_eu) {
      $vars['is_eu'] = TRUE;
    } else {
      $vars['is_eu'] = FALSE;
    }
  }
}

/**
 * Get the resource type.
 *
 * @param array $vars
 *   The variables array.
 *
 * @return string
 *   The resource type.
 */
function resource_type(array $vars) {
  if (isset($vars['node'])) {
    $node = $vars['node'];

    $is_object = is_object($node);

    if ($is_object) {
      $content_type = $node->bundle();
      if ($content_type == 'resource') {
        if ($node->hasField('field_resource_type_list')) {
          $field_resource_type_list = $node->get('field_resource_type_list');

          if (!($field_resource_type_list->isEmpty())) {
            $field_resource_type_list->first();
            $field_resource_type_list = $field_resource_type_list->first();
            $resource_type = $field_resource_type_list->getValue();
            $resource_type = $resource_type['value'];
            return $resource_type;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_views_view_unformatted__product_type_sidebar(&$vars) {
  $term_id = $vars['rows'][0]['content']['#row']->taxonomy_term_field_data_taxonomy_term__field_industry_tid;

  if (!empty($term_id)) {
    $term = Term::load($term_id);
    $name = $term->getName();
    $vars['industry_name'] = $name;
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_views_view__sales_literature_resources__resources_landing_filter(&$vars) {
  _follett_preprocess_views_selective_filters($vars);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_views_view__resources_tech_documentation__tech_documentation_page(array &$vars) {
  _follett_preprocess_views_selective_filters($vars);
}

/**
 * Reusable function for preprocessing views that use views_selective_filters.
 *
 * @param array $vars
 *   The vars passed to the preprocess function.
 */
function _follett_preprocess_views_selective_filters(array &$vars) {
  // By default, don't show results.
  $showResults = FALSE;

  // If the view is being loaded via ajax, show the results.
  $request = \Drupal::request();
  if ($request->isXmlHttpRequest()) {
    $showResults = TRUE;
  }

  // If the view is being loaded with filters populated already, show results.
  $view = $vars['view'];
  foreach ($view->filter as $filter) {
    if (get_class($filter) === 'Drupal\views_selective_filters\Plugin\views\filter\Selective') {
      if (isset($filter->options['group_info']) && isset($filter->options['group_info']['default_group']) && isset($view->exposed_raw_input[$filter->field])) {
        if ($filter->options['group_info']['default_group'] !== $view->exposed_raw_input[$filter->field]) {
          $showResults = TRUE;
          break;
        }
      }
    }
  }

  // Set a variable to use in the twig template.
  $vars['view_show_results'] = $showResults;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function follett_preprocess_block(&$vars) {

  $elements = $vars['elements'];

  if ($elements['#id'] == 'footerpagelookupformblock') {
    // Turn off caching for this block.
    $vars['#cache']['max-age'] = 0;
  }

  $host = \Drupal::request()->getHost();
  $vars['base_url'] = $host . '/';
  $node = \Drupal::routeMatch()->getParameter('node');

  if ($node && is_object($node)) {
    // You can get nid and anything else you need from the node object.
    $vars['node_id'] = $node->id();
  }
}

/**
 * Apply the necessary preprocessing functions.
 *
 * @param string $preprocess_type
 *   The entity type to preprocess.
 * @param string $preprocess_key
 *   The content type to preprocess.
 * @param array $funcs
 *   A list of callbacks to run.
 * @param array $vars
 *   The variables array to use while preprocessing.
 * @param string $hook
 *   The hook to run after preprocessing.
 */
function _follett_apply_preprocess_funcs($preprocess_type, $preprocess_key, array $funcs, array &$vars, $hook = NULL) {

  if (preg_match('/^[A-Za-z0-9\-_]+$/', $preprocess_type)) {

    $preprocess_files[] = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'preprocess' . DIRECTORY_SEPARATOR . 'preprocess__' . $preprocess_type . '.inc';
    if ($preprocess_key && preg_match('/^[A-Za-z0-9\-_]+$/', $preprocess_key)) {
      $preprocess_files[] = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'preprocess' . DIRECTORY_SEPARATOR . 'preprocess__' . $preprocess_type . '__' . $preprocess_key . '.inc';
      $preprocess_files[] = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'helpers__' . $preprocess_type . '__' . $preprocess_key . '.inc';
    }

    foreach ($preprocess_files as $preprocess_file) {
      if (is_readable($preprocess_file)) {
        include_once $preprocess_file;
      }
    }
    foreach ($funcs as $func) {
      if (function_exists($func)) {
        if ($hook) {
          $func($vars, $hook);
        }
        else {
          $func($vars);
        }
      }
    }
  }
}
