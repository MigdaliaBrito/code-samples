<?php
  function follett_preprocess__node__industry_landing(&$vars) {
    $node = $vars['node'];

    if ($node->hasField('field_product_type_subcategory')) {
      $field_product_type_subcategory = $node->get('field_product_type_subcategory');

      if (!empty($field_product_type_subcategory)) {
        $amount = $field_product_type_subcategory->count();

        if ($amount % 2 == 0) {
          $vars['is_even'] = 'even';
        }
      }
    }

    if ($node->hasField('field_solutions')) {
      $field_solutions = $node->get('field_solutions');
      $three_col_group = array(5, 6, 9);
      $four_col_group = array(7, 8);
      $placeholder = array(7, 5);


      if (!($field_solutions->isEmpty())) {
        $amount = $field_solutions->count();

        if (in_array($amount, $three_col_group)) {
          $vars['columns'] = 'three-col';
        }

        if (in_array($amount, $four_col_group)) {
          $vars['columns'] = 'four-col';
        }

        if (in_array($amount, $placeholder)) {
          $vars['placeholder'] = TRUE;
        }

        if ($amount == 10) {
          $vars['columns'] = 'five-col';
        }
      }
    }
  }
