<?php


function follett_preprocess__node__service_agent(&$vars) {
  $node = $vars['node'];

  if (is_object($node)) {

    if ($node->hasField('field_phone_number')) {
      $field_phone_number = $node->get('field_phone_number');

      if (!$field_phone_number->isEmpty()) {
        $vars['phone_number'] = $field_phone_number->getString();
      }
    }

    if ($node->hasField('field_fax_number')) {
      $field_fax_number = $node->get('field_fax_number');

      if (!$field_phone_number->isEmpty()) {
        $vars['fax_number'] = $field_fax_number->getString();
      }
    }
  }
}