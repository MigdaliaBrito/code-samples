<?php


function follett_preprocess__node__seminars(&$vars) {
  $node = $vars['node'];

  if (is_object($node)) {

    if ($node->hasField('field_seminar_date')) {
      $field_seminar_date = $node->get('field_seminar_date');

      if (!$field_seminar_date->isEmpty()) {
        $field_seminar_date = $field_seminar_date->first();

        $field_seminar_value = $field_seminar_date->getValue();
        $field_seminar_value = $field_seminar_value['value'];

        $seminar_date = strtotime($field_seminar_value);
        $current_date = time();

        if ($seminar_date < $current_date) {
          $vars['is_past'] = true;
        }
      }
    }
  }
}