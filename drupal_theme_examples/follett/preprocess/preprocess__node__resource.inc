<?php
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

function follett_preprocess__node__resource(&$vars) {
  $node = $vars['node'];

  // see function in .theme file
  $vars['resource_type'] = resource_type($vars);
  $referer = \Drupal::request()->server->get('HTTP_REFERER');

  if ($referer != '/') {
    $vars['referer'] = $referer;
  }
}