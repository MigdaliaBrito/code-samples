<?php
  use \Drupal\Core\Url;
  use Drupal\taxonomy\Entity\Term;
  use \Drupal\taxonomy;
  use Drupal\Core\Entity;

 function follett_preprocess__node__product(&$vars) {
   $node = $vars['node'];
   $current_path = \Drupal::service('path.current')->getPath();


   if (strpos($current_path, 'taxonomy')) {
     $delimiter = "/";
     $delimited_path = explode($delimiter, $current_path);
     $product_type_tid = end($delimited_path);
     $product_type_object = Term::load($product_type_tid);
     $vars['product_type_tid'] = $product_type_tid;

     if ($product_type_object->hasField('field_industry')) {
       $field_industry = $product_type_object->get('field_industry')->first();
       $field_industry_values = $field_industry->getValue();
       $industry_tid = $field_industry_values['target_id'];
       $vars['industry_tid'] = $industry_tid;
     }
   }

   if ($node && is_object($node)) {
     $num_slides = $node->get('field_product_slideshow')->getEntity()->get('field_product_slideshow')->count();

     if (!empty($num_slides)) {
      if ($num_slides > 0 && $num_slides > 1) {
        $vars['num_slides'] = $num_slides - 1;
      }
     }

     $is_field_product_type_empty = $node->get('field_product_type')->isEmpty();

     if (!$is_field_product_type_empty) {

      $product_term = Term::load($node->get('field_product_type')->target_id);

      if ($product_term) {
        $tid = $product_term->id();
        $storage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');

        $parents = $storage->loadParents($tid);
        $vars['product_type_url'] = $product_term->toUrl();

        if (!empty($parents)) {
          $parents = reset($parents);
          $vars['parent_name'] = $parents->getName();
        }

        else {
          $product_term_fields = $product_term->getFields();
          $field_optional_title = $product_term_fields['field_optional_title'];

          if (!$field_optional_title->isEmpty()) {
            $product_term_title = $field_optional_title->first()->getValue();
            $vars['product_type_name'] = $product_term_title['value'];
          } else {
            $vars['product_type_name'] = $product_term->getName();
          }
        }
      }
     }

     //show accordion if any of these fields are not empty
     $accordion_fields = [
       'field_product_specifications',
       'field_accessories',
       'field_related_model_numbers',
       'field_product_related_content'
     ];

     $show_accordion = FALSE;
     foreach($accordion_fields as $field) {
       if (!$show_accordion) {
         $is_empty = $node->get($field)->isEmpty();

         if (!$is_empty) {
           $show_accordion = TRUE;
           $vars['show_accordion'] = TRUE;
         }
       }
     }

     if ($node->hasField('field_product_slideshow')) {
       $slideshow_image_array = array();
       $field_product_slideshow = $node->get('field_product_slideshow');

       if (!$field_product_slideshow->isEmpty()) {
         $referenced_entities = $field_product_slideshow->referencedEntities();

         foreach ($referenced_entities as $entity) {
           $file_entity = $entity->field_product_slideshow_images->referencedEntities();

           if (!empty($file_entity)) {
            $file_uri = $file_entity[0]->getFileUri();

            if (!empty($file_uri)) {
             $file_uri = $file_entity[0]->getFileUri();
             $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('slider_thumbnail');
             $image_style_url = $style->buildUrl($file_uri);
             array_push($slideshow_image_array, $image_style_url);
            }
           }
         }
       }

       if (!empty($slideshow_image_array)) {
         $vars['thumbs_url'] = $slideshow_image_array;
       }
     }

     if ($node->hasField('field_hide_sales_lit_link')) {
       $field_hide_sales_lit_link = $node->get('field_hide_sales_lit_link');

       if (!$field_hide_sales_lit_link->isEmpty()) {
         $field_value = $field_hide_sales_lit_link->first()->getValue('value');

         $field_value = $field_value['value'];

         if ($field_value == '1') {
           $vars['hide_sales_link'] = true;
         }
       }
     }

   }
 }
