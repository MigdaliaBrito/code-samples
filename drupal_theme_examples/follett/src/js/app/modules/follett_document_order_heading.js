import $ from 'jquery';

class DocumentOrderHeading {
  static init(context) {
    let $table_input = $('#edit-document-order-items input[type="text"]');
    let $label;

    $table_input.each(function() {
      $label = $(this).val();

     $(this).prev().text($label);
    });

  }
}

module.exports = DocumentOrderHeading;
