import $ from 'jquery';
import Vivus from 'vivus';
import 'imagesloaded';

class heroAnimation {
  static init(context) {
    if ($('.homepage-hero__boundary').length) {
      new Vivus('follett_f', {type: 'sync', duration: 180, animTimingFunction: Vivus.EASE});
    
      $('.homepage-hero__boundary').addClass('animate');
      var icon_delay = 1.5;
      $('.homepage-hero__menu .menu_link__icon').each(function() {
        $(this).css('transition-delay', icon_delay + 's');
        icon_delay += .3;
      });

      var text_delay = 1.6;
      $('.homepage-hero__menu .menu_link__title').each(function() {
        $(this).css('transition-delay', text_delay + 's');
        text_delay += .3;
      });
    }
  }
}

module.exports = heroAnimation;
