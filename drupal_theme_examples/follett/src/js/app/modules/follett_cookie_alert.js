'use strict';
import $ from 'jquery';
import * as Cookies from "js-cookie";

class cookieAlert {
  static init(context) {
    let $placeholder_container = $('.alert--cookie').next();

    if ( $('.alert--cookie').length ) {
      setHeight();

      let $cookie = Cookies.get('cookie_accepted');


      if ($cookie === undefined ||  $cookie === null) {
        $('.alert--cookie').addClass('show');
        $placeholder_container.addClass('show');
      }

      $( '.alert--cookie .alert__button' ).click(function() {
        Cookies.set('cookie_accepted', 'true', {
          expires: 360
        });

        $('.alert--cookie').removeClass('show');
        $placeholder_container.removeClass('show');
      });


      $( window ).resize(function() {
        clearTimeout(window.resizedFinished);
        window.resizedFinished = setTimeout(function(){
          setHeight();
        }, 250);
      });
    }

    if ( $('.eu_visitor').length ) {
      let $cookie = Cookies.get('cookie_eu');

      if ($cookie === undefined ||  $cookie === null) {
        $('.alert--europe').addClass('show');
      }

      $( '.alert--europe .alert__button' ).click(function() {
        Cookies.set('cookie_eu', 'true', {
          expires: 360
        });
        $('.alert--europe').removeClass('show');
      });
    }

    function setHeight() {
      let height = $( '.alert--cookie' ).height();
      $placeholder_container.height(height);
    }
  }
}

module.exports = cookieAlert;
