
import $ from 'jquery';

class ServiceAgentLookup {
  static init(context) {
    let options = {
      scroll_to: $('.location-services--form .views-element-container'),
      location: $('.location-services--form'),
      form: $('#views-exposed-form-service-agent-lookup-page-1'),
      throbber_html: '<div class="ajax-throbber__boundary"><div class="ajax-throbber sk-rotating-plane"></div></div>'
    };


    // Checks to see if there is a query string in the url
    if (options.location.length) {
      if (window.location.search) {
        $('html, body').animate({
            scrollTop:options.scroll_to.offset().top
        }, 1000);
      }
    }

    options.form.submit(function() {
      $('body').append(options.throbber_html);
    });
  }
}

module.exports = ServiceAgentLookup;
