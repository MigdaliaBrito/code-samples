import $ from 'jquery';

class header_search {
  static init(context) {
    var button_container = ($('.desktop-search__button'));

    button_container.unbind().click(function() {
      $('.desktop-search__boundary').toggleClass('active');
      $('.header-bottom').toggleClass('slide-down');
    });

    // click off, close search 
    $('html').unbind().click(function(e) {
      //if clicked element is not your element and parents aren't your div

      if (!$(e.target).hasClass('search-form__desktop-header') && $(e.target).parents('.search-form__desktop-header').length == 0 && !$(e.target).hasClass('desktop-search__button')) {
        //do stuff
        $('.desktop-search__boundary').removeClass('active');
        $('.header-bottom').removeClass('slide-down');
      }
    });
  }
}

module.exports = header_search;
