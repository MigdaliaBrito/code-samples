import $ from 'jquery';

class productGrid {
  static init(context) {
    var $quick_preview_link = $('.product-list-grid__teaser, .product-list-grid__close-btn');
    var $close_quick_preview = $('.product-list-grid__close-btn');
    var resizeTimer;

    $quick_preview_link.click(function(event) {
      quickPreviewOpen($(this));
    });

    $(window).resize(function() {
      clearTimeout(resizeTimer);

      resizeTimer = setTimeout(function() {
        closeAllItems();
      }, 250);
    });

    function quickPreviewOpen(that) {
      var $product_item_container = that.parents('.product-list-grid__item');

      $product_item_container.toggleClass('open');
      $product_item_container.find('.product-list-grid__detail')
        .slideToggle();
      $product_item_container.find('.product-list-grid__quick-preview__link')
        .toggleClass('active');
    }

    function closeAllItems() {
      $('.product-list-grid__item.open .product-list-grid__detail').slideToggle();
      $('.product-list-grid__item').removeClass('open');
      $('.product-list-grid__quick-preview__link').removeClass('active');
    }
  }
}

module.exports = productGrid;
