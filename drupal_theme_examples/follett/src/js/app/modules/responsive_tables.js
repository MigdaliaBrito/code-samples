import $ from 'jquery';

class responsiveTables {

  static init(context) {
    var $tables = $('.paragraph--type--body-copy table, #edit-document-order-items');

    if ($tables.length) {
      $tables.each(function() {
        var table_headers_array = [];
        var table_headers_values,
          table_rows;

        table_headers_values = $(this).find('th');

        // make array of th and store in array
        if(table_headers_values.length) {
          table_headers_values.each(function() {
            table_headers_array.push($(this).text());
          })
        }

        table_rows = $(this).find('tr');

        // TODO: check if there are any table rows with .length

        table_rows.each(function(index) {
          $(this).children('td').each(function(index) {
            $(this).attr('data-th', table_headers_array[index]);
          });
        });
      }); //end table each
    }
  }
}


module.exports = responsiveTables;
