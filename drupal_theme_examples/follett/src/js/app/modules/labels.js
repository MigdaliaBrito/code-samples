'use strict';

import $ from 'jquery';

class labels {
  static init() {
    var selects = [
      { id: '#edit-field-resource-industry-target-id', 'type': 'terms' },
      { id: '#edit-field-resource-product-type-target-id-selective', 'type': 'terms' },
      { id: '#edit-field-associated-products-target-id-selective', 'type': 'products' },
      { id: '#edit-field-documentation-type-target-id-selective', 'type': 'terms' },
      { id: '#edit-field-language-target-id-selective', 'type': 'terms' }
    ];

    $.each($(selects), function(select_index, data) {
      $.each($(data.id).children(), function(option_index, option) {
        if ($(option).val() == 'All') {
          return;
        }

        if (drupalSettings.labels[data.type].hasOwnProperty($(option).val())) {
          if (drupalSettings.labels[data.type][$(option).val()] != option.innerText) {
            option.innerText = drupalSettings.labels[data.type][$(option).val()];
          }
        } else {
          // if the index isn't found, remove it from the list
          $(data.id + ' option[value="' + $(option).val() + '"]').remove();
        }
      });
      $(data.id).data('selectBox-selectBoxIt').refresh();
    });
  }
}

module.exports = labels;
