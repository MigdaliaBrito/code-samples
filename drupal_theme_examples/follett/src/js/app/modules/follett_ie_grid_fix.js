import $ from 'jquery';

class IeFix {
  static init(context) {
    let options = {
      grid_container: $('.product-list-grid'),
      grid_item: $('.product-list-grid .views-row')
    };
    if (window.matchMedia("(min-width: 700px)").matches) {
      // loop through and put each grid item in its proper column and row
      //start column 1
      //start row 1

      let row = 1;
      let column = 1;
      let iterator = 1;

      if (options.grid_container.length) {
        options.grid_item.each(function(index, element) {
          if (iterator == 1) {
            $(this).css('-ms-grid-column', '1');
            $(this).css('-ms-grid-row', row.toString());
          }

          if (iterator != 1) {
            column += 2;
            $(this).css('-ms-grid-column', column.toString());
            $(this).css('-ms-grid-row', row.toString());
          }

          iterator ++;

          // every 7th column, break into new row
          if (column % 7 == 0) {
            row++;
            column = 1;
            iterator = 1;
          }
        });
      }
    }
  }
}

module.exports = IeFix;
