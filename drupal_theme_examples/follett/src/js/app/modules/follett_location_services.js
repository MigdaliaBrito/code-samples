import $ from 'jquery';

class LocationServices {
  static init(context) {
    let options = {
      button: $('.location-services__btn'),
      address_field: $('#edit-field-postal-codes-serviced-value'),
      form: $('#views-exposed-form-service-agent-lookup-page-1')
    };

    let geocoder;

    options.button.click(function() {
      geocoder = new google.maps.Geocoder();

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          let latlng = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
          geocodeLatLng(geocoder, latlng);
        });
      } else {
        /* geolocation IS NOT available */
      }
    });


    function geocodeLatLng(geocoder, latlng) {
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
          if (results[0]) {
            let geolocated_address = results[0].address_components[7].short_name;
            options.address_field.val(geolocated_address);
            options.form.submit();
          }
        }
        else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });
    }
  }
}

module.exports = LocationServices;
