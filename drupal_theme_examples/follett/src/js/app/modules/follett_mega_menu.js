import $ from 'jquery';

// TODO: make all of this way better and modular

class megaMenu {
  static init(context) {
    menuHover();
    hoverOut();

    function menuHover() {
      $('.navigation__primary--desktop a').hover(function() {

        if ( $('.header-bottom.slide-down').length == 0 ) {
          $('.menu__link').removeClass('hover');
          $(this).addClass('hover');
  
          if ($('.mega-menu.hover').length) {
            $('.mega-menu__boundary').removeClass('hover');
            $('.mega-menu').removeClass('selected');
  
            if ($(this).data('menu-link') == 'foodservice') {
              $('.mega-menu-foodservice .mega-menu__boundary').addClass('hover');
              $('.mega-menu-foodservice .mega-menu').addClass('selected');
            }
    
            if ($(this).data('menu-link') == 'healthcare') {
              $('.mega-menu-healthcare .mega-menu__boundary').addClass('hover');
              $('.mega-menu-healthcare .mega-menu').addClass('selected');
            }
    
            if ($(this).data('menu-link') == 'workplace') {
              $('.mega-menu-workplace .mega-menu__boundary').addClass('hover');
              $('.mega-menu-workplace .mega-menu').addClass('selected');
            }
          }
  
          else {
            $('.mega-menu', '.header-bottom-hover').addClass('hover');
            $('.header-bottom-hover').addClass('hover');
  
            if ($(this).data('menu-link') == 'foodservice') {
              $('.mega-menu-foodservice .mega-menu__boundary').addClass('hover');
              $('.mega-menu-foodservice .mega-menu').addClass('selected');
            }
    
            if ($(this).data('menu-link') == 'healthcare') {
              $('.mega-menu-healthcare .mega-menu__boundary').addClass('hover');
              $('.mega-menu-healthcare .mega-menu').addClass('selected');
            }
    
            if ($(this).data('menu-link') == 'workplace') {
              $('.mega-menu-workplace .mega-menu__boundary').addClass('hover');
              $('.mega-menu-workplace .mega-menu').addClass('selected');
            }
          }
        }
      });
    }

    function hoverOut() {
      $('main, .custom-block__image-boundary, .navigation__secondary--desktop a').hover(function() {
        $('.mega-menu__boundary').removeClass('hover');
        $('.mega-menu').removeClass('hover').removeClass('selected');
        $('.header-bottom-hover').removeClass('hover');
        $('.navigation__primary--desktop a').removeClass('hover');
      });

      $('.desktop-search__boundary').hover(function() {
        $('.mega-menu__boundary').removeClass('hover');
        $('.mega-menu').removeClass('hover').removeClass('selected');
        $('.header-bottom-hover').removeClass('hover');
        $('.navigation__primary--desktop a').removeClass('hover');
      });
    }
  }
}

module.exports = megaMenu;
