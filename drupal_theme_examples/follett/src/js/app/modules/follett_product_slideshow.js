import $ from 'jquery';
import jQueryBridget from "jquery-bridget";
import Flickity from "flickity";
require('flickity-as-nav-for');
//importing flickity as a plugin
jQueryBridget('flickity', Flickity, $ );

class productSlideshow {
  static init(context) {
    var $flickity_container_main = $('.product-detail__slideshow');
    var $flickity_container_nav =  $('.product-detail__slideshow-nav');

    if ($flickity_container_main.length) {
      $flickity_container_main.flickity({
        pageDots: false,
        wrapAround: true,
        prevNextButtons: false
      });
    }

    if ($flickity_container_nav.length) { 
      $flickity_container_nav.flickity({
        asNavFor: '.product-detail__slideshow',
        contain: true,
        prevNextButtons: false,
        pageDots: false
      });
    }
  }
}

module.exports = productSlideshow;
