import $ from 'jquery';

class mobile_menu {
  static init(context) {
    var click_container = $('.header__hamburger', context);
    var hamburger_container = $('.hamburger__boundary');
    var hamburger_text = $('.header__hamburger-text');
    var header_navigation = $('.header-navigation__boundary');


    click_container.unbind().click(function(event) {
      hamburger_container.toggleClass('close');
      hamburger_text.toggleClass('active');
      header_navigation.toggleClass('slideInLeft');
      $('main').toggleClass('blur');
      $('body').toggleClass('no-scroll');
      $('html').toggleClass('no-scroll');
    });
  }
}

module.exports = mobile_menu;
