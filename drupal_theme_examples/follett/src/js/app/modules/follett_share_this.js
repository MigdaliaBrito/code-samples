import $ from 'jquery';


class follettShareThis {
  static init(context) {
    var $sharethis_container = $('.share-this__boundary');
    var $sharethis_toolbox = $('.share-this__toolbox');

    $sharethis_container.unbind('click');

    $sharethis_container.click(function() {
      $sharethis_toolbox.slideToggle(250);
    });
  }
}

module.exports = follettShareThis;
