import $ from 'jquery';
require('../vendor/jquery-ui-widget.js');
require('../vendor/jquery.selectBoxIt.min.js');

class selectBoxit {
  static init(context) {
    // Media queries
    let md_media = window.matchMedia('(min-width: 1050px)');


    if ($('.google-translate select').length) {
      $('.google-translate select').selectBoxIt();
    }

    if ($('select.model-finder-filter').length) {
      $('select.model-finder-filter').selectBoxIt();
    }

    if ($('.product-type select').length) {
      $('.product-type select').selectBoxIt({
        autoWidth: true
      });

      closeSelectBoxIt();
      window.addEventListener('resize', closeSelectBoxIt, false);
    }

    function closeSelectBoxIt() {
      let selectbox = $('.product-type select').selectBoxIt().data('selectBox-selectBoxIt');

      if (window.matchMedia('(min-width: 1050px)').matches) {
        selectbox.open();
      }

      else {
        selectbox.close();
      }
    }

    if ($('.webform-submission-form').length) {
      $('.webform-submission-form select').selectBoxIt({
        showFirstOption: false,
        autoWidth: false,
        copyClasses: "container"
      });
    }

    if ($('.resources__filter-boundary select').length) {
      $('.resources__filter-boundary select').selectBoxIt({
        autoWidth: false
      });
    }

    if ($('.model-number select').length) {
      $('.model-number select').selectBoxIt({
        autoWidth: false
      });
    }
  }
}

module.exports = selectBoxit;
