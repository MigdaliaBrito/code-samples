import $ from 'jquery';


// shows reset button upon input change in sales literature/sales documentation view
class DocumentationResetShow {
  static init(context) {
    let path = '/sales-literature';
    let current_path = window.location.pathname;
    let has_query_string = current_path.search('/sales-literature?');

    // See if user is coming to url having already selected filters
    if (has_query_string == 0) {
      $('.input__boundary__input-type-reset').addClass('show');

      if ($('.input__boundary__input-type-reset').length) {
        $('.resources__filter-boundary').addClass('show');
      }
    }

    // show reset when a user has modified a select
    $('#views-exposed-form-sales-literature-resources-resources-landing-filter select').on('change', function() {
      if ($('.input__boundary__input-type-reset').length) {
        $('.input__boundary__input-type-reset').addClass('show');
      }
    });
  }
}

module.exports = DocumentationResetShow;
