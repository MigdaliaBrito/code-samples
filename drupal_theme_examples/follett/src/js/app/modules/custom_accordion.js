import $ from 'jquery';


/* HTML for accordion:
 * <dl class="accordion">
 * <dt class="accordion-toggle">Toggle</dt>
 * <dd class="accordion-content">Content</dd>
 * </dl>
*/


class customAccordion {
  static init(context) {
    // accordionFirstClick = false;
    $('.accordion-content').once().css('display', 'none');

    // $('.accordion dt').first().addClass('active');
    // $('.accordion dd').first().addClass('active').css('display', 'block');

    // Accordion
    $('.accordion .accordion-toggle').unbind().on('click', function(e){

      e.preventDefault();

      // if (this) not already open
      if (!$(this).hasClass('active')) {
        // if another panel is open
        if ($('.accordion dd').not($(this)).hasClass('active')) {
          $('.accordion dd').not($(this).next()).slideUp(300);
        }

        $('.accordion dt, .accordion dd').removeClass('active');
        $('.accordion-toggle__icon').removeClass('close');

        $(this).find('.accordion-toggle__icon').addClass('close');
        $(this).addClass('active');
        $(this).next().addClass('active');
        $(this).next().slideToggle(300, function() {
          // Scroll to top of active element
          $('html, body').animate({
              scrollTop: $(this).offset().top - 200
          }, 200);
          $(this).css('display', 'block');
        });

      // if (this) already open, just close
      } else {

        $('.accordion dt, .accordion dd').removeClass('active');
        $('.accordion-toggle__icon').removeClass('close');
        $(this).next().slideUp(300);
      }

    });
  }
}

module.exports = customAccordion;
