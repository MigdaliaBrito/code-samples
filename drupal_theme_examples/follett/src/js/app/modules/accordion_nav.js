import $ from 'jquery';
import utils from './utils.js';

/***
*  @function: Drupal.behaviors.tabs
*  @description:  Tab plugin
***/

class accordion_nav {
  static init(context) {

    var options = {
      'elBtnExpand' : $('.menu-collapsed__menu button.expand'),
      'elActiveTrail' : $('.menu-collapsed__menu li.active-trail')
    };

    // clear active trail classes
    $('.menu-collapsed__menu li').removeClass('active-parent');
    $('.menu-collapsed__menu button').removeClass('active-btn');

    // open active trail
    options.elActiveTrail.addClass('active-parent');
    options.elActiveTrail
      .find('> .link-container > button')
      .addClass('active-btn');
    options.elActiveTrail.find('> .sub-menu').show();

    // event handler
    options.elBtnExpand.unbind().click(function() {
      var btn = $(this);
      var targetMenu = $(this).parent().next();
      var parentContainer = $(this).parent().parent();

      // if $this is active, close and gtfo
      if (btn.hasClass('active-btn')) {
        targetMenu.slideUp(300);
        btn.removeClass('active-btn');
        parentContainer.removeClass('active-parent');
        return;
      }


      // if the btn clicked is not a child, close any other open menu at the same level
      if (parentContainer.siblings().hasClass('active-parent')) {
        parentContainer.siblings().find('.sub-menu').slideUp(300);
        parentContainer.siblings().removeClass('active-parent');
        parentContainer.siblings().find('button').removeClass('active-btn');
        parentContainer.siblings().find('li').removeClass('active-parent');
      }

      // open the target menu
      targetMenu.slideDown(300);
      parentContainer.addClass('active-parent');
      btn.addClass('active-btn');
    });


    // Mobile: close button
    $('.mobile-nav-close, .mobile-overlay').on('click', function(e) {
      e.preventDefault();
      $('.menu-collapsed__menu').removeClass('active');
      $('.mobile-overlay').removeClass('active');
    });

  }
}

module.exports = accordion_nav;
