'use strict';
import $ from 'jquery';

class customVideoThumbnail {
  static init(context) {
    var $thumbnail = $('.paragraph__video-callout__poster-image');
    $thumbnail.click(function() {
      $(this).fadeOut(1000, function() {
        var this_attribute = $(this).next().attr('src');
        $(this).next().attr('src', this_attribute + '&amp;autoplay=1');
      });
    });
  }
}

module.exports = customVideoThumbnail;
