import $ from 'jquery';


class follettTallestTitle {
  static init(context) {
    var maxHeight = -1;

    if ($('.related-products__boundary'.length)) {
      $('.related-products__boundary .field_product_display_title').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
      });
  
      $('.related-products__boundary .field_product_display_title').each(function() {
        $(this).height(maxHeight);
      });
    }
  }
}

module.exports = follettTallestTitle;
