import $ from 'jquery';

class backToTop {
  static init(context) {
    if($('.layout-content--has-sidebar').length) {
      $('.back-to-top').addClass('sidebar');
    }

    $('.back-to-top .body').click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
    });
  }
}

module.exports = backToTop;
