'use strict';

import $ from 'jquery';
import './vendor/bootstrap.min.js';

Drupal.behaviors.follett_theme_init = {
  attach: function(context, settings) {

    var accordion_nav = require('./modules/accordion_nav.js');
    accordion_nav.init();
    var mobile_menu = require('./modules/follett_mobile_menu.js');
    mobile_menu.init();
    var header_search = require('./modules/follett_header_search.js');
    header_search.init();
    var responsive_tables = require('./modules/responsive_tables.js');
    responsive_tables.init();
    var custom_accordion = require('./modules/custom_accordion.js');
    custom_accordion.init();
    var video_poster_play = require('./modules/video_poster_play.js');
    video_poster_play.init();
    var follett_selectboxit = require('./modules/follett_selectboxit.js');
    follett_selectboxit.init();
    var scroll_to_top = require('./modules/back_to_top.js');
    scroll_to_top.init();
    var homepage_hero_animation = require('./modules/homepage_hero_animation.js');
    homepage_hero_animation.init();
    var share_this = require('./modules/follett_share_this.js');
    share_this.init();
    var product_grid = require('./modules/follett_product_grid.js');
    product_grid.init();
    var product_slideshow = require('./modules/follett_product_slideshow.js');
    product_slideshow.init();
    var mega_menu = require('./modules/follett_mega_menu.js');
    mega_menu.init();
    var document_order_heading = require('./modules/follett_document_order_heading.js');
    document_order_heading.init();
    var cookie_alert = require('./modules/follett_cookie_alert.js');
    cookie_alert.init();
    var location_services = require('./modules/follett_location_services.js');
    location_services.init();
    var ie_grid_fix = require('./modules/follett_ie_grid_fix.js');
    ie_grid_fix.init();
    var doc_reset_show = require('./modules/documentation_reset_show.js');
    doc_reset_show.init();
    var service_agent_lookup = require('./modules/service_agent_lookup.js');
    service_agent_lookup.init();

    //Runs everytime Ajax is submitted
    $(document).ajaxComplete(function (e, xhr, settings) {
      if ($('.resources-teaser__share-this').length) {
        $('.resources-teaser__share-this').addClass('show');
      }
    });
  }
}
