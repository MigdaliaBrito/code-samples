
@SETUP
- run in console from theme folder:

  npm install gulp -g
    (run this once, initially)

  npm install
    (run this once, initially)

  gulp
    (to run all scripts)


@DEFAULT FILE STRUCTURE

Javascript source files
/src/js

SASS source files
/src/scss

Javascript production files
/dist/js

SASS production files
/dist/css

Drupal templates
/tpl

Drupal preprocess files
/preprocess


@FEATURES
- SASS: bourbon & neat included (feel free to remove neat on projects that don’t need to support IE9)
- SASS: sass partial mapping (browser inspector will show which partial the selector originated from
- SASS: bulk import enabled (import a folder instead of each damned file. make sure to restart gulp after adding a file)
- SASS: on sass error, gulp watch will not crash
- JS: transpiling using the “latest” language spec instead of a specific version (currently ecmascript 2015)
- JS: Linting enabled
- JS: On JS error, gulp watch will not crash
- JS: Modernizr enabled, but no options added by default (customization available in gulpfile.js.)


@TODO
- add Browsersync (if needed)
- add the html and css for the accordion nav