## Gulp File
### Installation

 - Put `gulp.js` and `package.json` in theme directory.
 - Run `npm install`
 - Run `gulp` to watch for any `CSS` or `JS` changes.

Gulp File Structure

    |- src
      |-- scss
	    |--- styles.scss
	    |--- components
	        |---- _partial.scss
	  |-- js
	    |--- app.js
	    |--- modules
	        |---- Module.js

## Features
`Sass` imports can be imported using globs. Example:

    @import  'components/*';
